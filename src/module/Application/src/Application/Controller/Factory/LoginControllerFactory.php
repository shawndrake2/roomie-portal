<?php

namespace Application\Controller\Factory;

use Application\Controller\LoginController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class LoginControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $sm)
    {
        $userMapper = $sm->getServiceLocator()->get('Database\User\UserMapper');
        return new LoginController($userMapper);
    }
}