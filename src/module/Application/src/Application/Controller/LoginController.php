<?php

namespace Application\Controller;

use Application\Utility\Login;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use Database\User\UserMapper;

class LoginController extends AbstractActionController
{
    /** @var UserMapper */
    private $dbMapper;

    public function __construct(UserMapper $mapper)
    {
        $this->dbMapper = $mapper;
    }

    public function indexAction()
    {
        if(Login::isLoggedIn()) {
            $this->redirect()->toRoute('home');
        } else {
            $view = new ViewModel();
            $view->setTerminal(true);

            return $view;
        }
    }

    public function postAction()
    {
        $params = $this->params()->fromPost();

        if( $this->compareLogin($params))
        {
            $sessionUser = new Container('user');
            $sessionUser->username = $params['username'];
            $sessionUser->loggedIn = true;
            $this->redirect()->toRoute('home');
        }
        else{

             $this->redirect()->toRoute('login');
        }
    }

    public function logoutAction()
    {
        $sessionUser = new Container('user');
        $sessionUser->getManager()->getStorage()->clear('user');
        $this->redirect()->toRoute('login');
    }

    private function compareLogin($params)
    {
        try {
            $user = $this->dbMapper->findByEmail($params['username']);
            return ($user->getPassword() == $params['password']);
        } catch (\Exception $e) {
            return false;
        }
    }

}






