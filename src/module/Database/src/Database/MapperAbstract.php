<?php
namespace Database;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

abstract class MapperAbstract
{
	/** @var EntityManager */
    private $entityManager;
	
    /** @var EntityRepository */
    private $repository;

    /**
     * @param EntityManager $entityManager
     * @param string $repositoryLocation
     */
    public function __construct(EntityManager $entityManager, $repositoryLocation)
    {
        $this->setEntityManager($entityManager);
        $this->setRepository($entityManager->getRepository($repositoryLocation));
    }

    /**
     * @return EntityRepository
     */
    public function getRepository()
    {
    	return $this->repository;
    }
    
    /**
     * @param EntityRepository $repository
     */
    public function setRepository(EntityRepository $repository)
    {
    	$this->repository = $repository;	
    }
    
    /**     
     * @return array
     */
    public function fetchAll()
    {
    	return $this->repository->findAll();       
    }

    /**
     * @param mixed $parentKey
     * @return mixed
     */
    public function findByParentKey($parentKey)
    {
        return $this->repository->find($parentKey);
    }

    /**
     * @param mixed $entity
     */
    public function saveEntity($entity)
    {
        $entityManager = $this->getEntityManager();
    	$entityManager->persist($entity);
    	$entityManager->flush();
    }

    /**
     * @param mixed $entity
     */
    public function deleteById($entity)
    {
        $entityManager = $this->getEntityManager();
        $entityManager->remove($entity);
        $entityManager->flush();
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
    	return $this->entityManager;
    }

    /**
     * @param EntityManager $entityManager
     */
    public function setEntityManager(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    
}
