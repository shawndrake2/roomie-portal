<?php
namespace Database\User\Factory;

use Database\User\UserMapper;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\FactoryInterface;

class UserMapperFactory implements FactoryInterface
{
	/**
	 * @param ServiceLocatorInterface $sm
     * @return UserMapper
	 */
	public function createService(ServiceLocatorInterface $sm)
	{
        /** @var \Doctrine\ORM\EntityManager $entityManager */
		$entityManager = $sm->get('doctrine.entitymanager.orm_default');
		$userMapper = new UserMapper($entityManager , 'Database\User\UserEntity');
		return $userMapper;
	}	
}