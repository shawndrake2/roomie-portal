<?php
namespace Database\User;

use Database\MapperAbstract;

class UserMapper extends MapperAbstract
{

    /**
     * @param $id
     * @return \Database\User\UserEntity
     */
    public function findById($id)
    {
        return $this->findByParentKey($id);
    }

    /**
     * @param Array $query
     * @return Array
     */
    public function findUsers($query)
    {
        return $this->getRepository()->findBy($query);
    }

    /**
     * @param string $email
     * @return \Database\User\UserEntity
     */
    public function findByEmail($email)
    {
        return $this->getRepository()->findOneBy(array('email' => $email));
    }
}