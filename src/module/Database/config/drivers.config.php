<?php
return array(
    'UserEntity_driver' => array(
        'class' => 'Doctrine\ORM\Mapping\Driver\XmlDriver',
        'cache' => 'array',
        'paths' => array(__DIR__ . '/xml')
    ),
    'orm_default' => array(
        'drivers' => array(
            'Database\User' => 'UserEntity_driver',
        )
    ),
);