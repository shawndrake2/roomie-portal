<?php

return array(
    'doctrine' => array(
        'driver' => require('drivers.config.php'),
    ),
    'service_manager' => array(
        'invokables' => array(
            'Database\User\UserEntity' => 'Database\User\UserEntity'
        ),
        'factories' => array(
            'Database\User\UserMapper' => 'Database\User\Factory\UserMapperFactory'
        ),
    ),
);
